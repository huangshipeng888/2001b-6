


export  type  RouteList=Array<RouteItem>

export interface RouteItem{
    path:string,
    name?:string,
    component?:any,
    children?:Array<RouteItem>,
    redirect?:string
}


export interface InitialState{

}

export interface Tp{
    type:string,
    payload:number|string|Array|object|boolean
}

export type Mydispatch=typeof store.dispatch