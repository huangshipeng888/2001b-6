import { lazy } from 'react';

const routers=[
    {
        path:"/home",
        name:"Home",
        component:lazy(()=>import('../views/home')),
        children:[
            {
                path:"/home/article",
                name:"article",
                component:lazy(()=>import("../views/article"))
            },
            {
                path:"/home/archive",
                name:"archive",
                component:lazy(()=>import("../views/archive"))
            },
            {
                path:"/home/knowlage",
                name:"knowlage",
                component:lazy(()=>import("../views/knowlage"))
            },
            {
                path:"/home/markdown",
                name:"markdown",
                component:lazy(()=>import("../views/markdown"))
            },
        ]
    },
    {
        path:"/",
        redirect:"/home"
    }
]

export default routers