import React, { Suspense } from 'react'
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom'
import routers from './RouterList'
import { RouteList } from '../utils/type'


const RouteViews = () => {
    const renderRouter=(routes:RouteList)=>{
        return routes.map((item,index)=>{
            return  <Route
                    path={item.path}
                    key={index}
                    element={item.redirect?<Navigate to={item.redirect}></Navigate>:<item.component/>}
                    >
                        {
                            item.children&&renderRouter(item.children)
                        }
                    </Route>
        })
    }
    return (
        <Suspense>
            <BrowserRouter>
                <Routes>
                    {
                        renderRouter(routers)
                    }
                </Routes>
            </BrowserRouter>
        </Suspense>
    )
}

export default RouteViews