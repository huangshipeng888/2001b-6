import { Dispatch } from "redux"
import { get_list } from "../api"

export const get_list_action=()=>{
    return async (dispatch:Dispatch)=>{
        const {data}=await get_list()
        console.log(data);
    }
}