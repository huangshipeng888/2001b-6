import { InitialState , Tp } from '../../utils/type'
const initialState:InitialState = {}

const store=(state = initialState, { type, payload }:Tp) => {
  switch (type) {

  case "":
    return { ...state, ...payload }

  default:
    return state
  }
}
export default store