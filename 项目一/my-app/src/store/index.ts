import { 
    legacy_createStore,
    applyMiddleware,
    combineReducers
 } from 'redux'

import logger from 'redux-logger'
import thunk from 'redux-thunk'
import store from './modules/user'

const reducer=combineReducers({
    store
})

export default legacy_createStore(reducer,applyMiddleware(thunk,logger))