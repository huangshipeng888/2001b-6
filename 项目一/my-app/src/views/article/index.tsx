type Props = {};
import "./article.css"
import {useDispatch} from "react-redux"
import { useEffect } from "react";
import { get_list_action } from "../../actions";
import { Mydispatch } from "../../utils/type";
const Article = (props: Props) => {
    const dispatch:Mydispatch=useDispatch()
    useEffect(()=>{
        dispatch(get_list_action())
    },[dispatch])
    return (
        <div className="article">
           <div className="con_box">
                <div className="con_left_box">
                    <div className="swiper_box">
                        轮播图盒子
                    </div>
                    <div className="article_box">
                        文章内容
                    </div>
                </div>
                <div className="con_right_box">
                    <div className="recommendToReading_box">
                        recommendToReading
                    </div>
                    <div className="tips_box">
                        tips
                    </div>
                </div>
           </div>
        </div>
    );
};

export default Article;
