import { Tabs } from 'antd';
import "./home.css"
import {Outlet,NavLink} from "react-router-dom"
type Props = {}

const index = (props: Props) => {
    const items=[
        {
            id:1,
            title:"article",
            path:"/home/article"
        },
        {
            id:2,
            title:"archive",
            path:"/home/archive"
        },
        {
            id:3,
            title:"knowlage",
            path:"/home/knowlage"
        },
        {
            id:4,
            title:"markdown",
            path:"/home/markdown"
        },
    ]
  return (
    <div className='index'>
       <header>
        <div className='tab_nav'>
            <img src="" alt="" className='photo'/>
            <Tabs defaultActiveKey="0">
                {
                    items.map((item,index)=>{
                        return <Tabs.TabPane tab={<NavLink to={item.path}>{item.title}</NavLink>} key={index}>
                      </Tabs.TabPane>
                    })
                }
            </Tabs>
            <div className='top_icon'>
                icon图标
            </div>
        </div>
       </header>
       <main>
        <div className='container'>
            <Outlet></Outlet>
        </div>
       </main>
       <footer></footer>
    </div>
  )
}

export default index