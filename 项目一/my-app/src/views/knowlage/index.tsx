import React from 'react'
import "./knowlage.css"
type Props = {}

const Knowlage = (props: Props) => {
  return (
    <div className='knowlage'>
      <div className="con_box">
        <div className="con_left_box">
          <div className='article_box'>
            文章内容
          </div>
        </div>
        <div className="con_right_box">
          <div className="recommendToReading_box">
            recommendToReading
          </div>
          <div className="category_box">
            category
          </div>
        </div>
      </div>
    </div>
  )
}

export default Knowlage